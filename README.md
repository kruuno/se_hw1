# Upute za korištenje kviza

## Autor: Krunoslav Kovač

1. Pokretanje Kviza
    - Prilikom pokretanja kviza otvara se izbornik koji nudi opcije izbora pokretanja kviza, dodavanja novog pitanja, ispisa rezultata ili izlazak iz kviza.
2. Početak kviza
    - Kviz započinjete unosom svojeg željenog imena
    - Zatim odgovarate na pitanja koja su zapisana u datoteci. Odgovorate na način tako da unosite jedno od ponudenih odgovora odnosno slovo ispred tocnog odgovora (npr. >>> a), također pripazite na CAPS LOCK.
    - Svako pitanje nosi 5 bodova što znači da svakim točnim odgovorom dobivate 5 bodova.
    - Svoj rezultat će te moći vidjeti na kraju kviza.
3. Dodavanje pitanja
    - Prilikom pokretanja opcije Dodavanja pitanja, prvo unosite željeno pitanje.
    - Nakon toga birate pod kojim slovom želite svoj točan odgovor. Unosite a, b, c ili d.
    - Nakon odabira slova pod kojim želite točan odgovor unosite odgovore. 
    - Pod onim slovom koji ste odabrali točan odgovor pojavit ce vam se "Ovdje unesite točan odgovor: " te kao što i traži će te postaviti točan odgovor. Kod ostalih će vam traziti samo unos nekog odgovora.
    - Kada sve korake odradite, pitanje će se zapisati u datoteku s pitanjima
    - Prilikom pokretanja također se rezultati izbrišu jer nam se povečava broj maksimalnog broja bodova kako bi lista bodova bila poštenija.
4. Ispis rezultata
    - Prilikom pokretanja opcije Ispis rezultata, ispisat će vam se 10 najboljih rezultata igrača (tzv. Hall of fame).