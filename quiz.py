print("####################### Dobro Dosli u kviz znanja #######################")

def dodaj_pitanje():
    #Za brisanje rezultata prilikom dodavanja novog pitanja
    with open('results.txt','w'): pass

    f=open("pitanja.txt", "a")
    add=raw_input("Dodaj pitanje: \n")
    f.write(add)
    add_Tocan=raw_input("Pod kojim slovom zelite tocan odgovor? (a/b/c/d)")
    f.write("_"+add_Tocan)

    if add_Tocan == "a":
        addA=raw_input("Ovdje unesite tocan odgovor: \n")
        f.write("_a)"+addA)
    else:
        addA=raw_input("Dodaj odgovor a: \n")
        f.write("_a)"+addA)
    
    if add_Tocan == "b":
        addB=raw_input("Ovdje unesite tocan odgovor: \n")
        f.write("_b)"+addB)
    else:
        addB=raw_input("Dodaj odgovor b: \n")
        f.write("_b)"+addB)

    if add_Tocan == "c":
        addC=raw_input("Ovdje unesite tocan odgovor: \n")
        f.write("_c)"+addC)
    else:
        addC=raw_input("Dodaj odgovor c: \n")
        f.write("_c)"+addC)
    
    if add_Tocan == "d":
        addD=raw_input("Ovdje unesite tocan odgovor: \n")
        f.write("_d)"+addD+"\n")
    else:
        addD=raw_input("Dodaj odgovor d: \n")
        f.write("_d)"+addD+"\n")

    f.close()

def pocetak_kviza():
    f=open("pitanja.txt", "r")
    red=f.readlines()
    bodovi=0
    count=0
    name= ""
    while name=="":
        name=raw_input("Unesite vase ime: ")

    for line in red:
        linija=line.split("_")
        tocanOdgovor=linija[1] #Postavlja tocan odgovor
        print(linija[0]) #printa pitanje
        print(linija[2]) #printa odgovor a
        print(linija[3]) #printa odgovor b
        print(linija[4]) #printa odgovor c
        print(linija[5]) #printa odgovor d
        unesiOdg=raw_input("Vas odgovor: ")
        if unesiOdg==tocanOdgovor:
            print("*************Tocan odgovor!*************\n")
            bodovi=bodovi+5
        else:
            print("*************Netocan odgovor!*************\n")
        count=count+5

    spremi_rezultat(bodovi,name)
    print("%s Vas broj ostvarenih bodova je: %d/%d \n")%(name,bodovi,count)
    f.close()

def spremi_rezultat(bodovi, name):
    with open("results.txt", "a ") as f:
        f.write(name+":_"+str(bodovi)+"_bodova.\n")
      
def ispis_rezultata():
    
    with open("results.txt", "r") as f:
        brojlinija=f.readlines()
        linija=[i.split("_") for i in f.readlines()]
    linija=sorted(linija,key=lambda x: int(x[1]),reverse=True)
    N=len(brojlinija)

    print("######### HALL OF FAME #########\n")
    f=open("results.txt", "r")
    if N<10:
        for i in range(N):
            line=f.next().replace("_"," ")
            print line
    else:
        for i in range(10):
            line=f.next().replace("_"," ")
            print line
    f.close()
    print("################################")
    
while True:
    izlaz=int(raw_input(">>>Unesi 1 za pocetak kviza.<<< \n>>>Unesi 2 za dodavanje pitanja.<<< \n>>>Unesi 3 za ispis rezultata.<<<\n>>>Unesi 0 za izlaz iz kviza.<<<\n"))
    
    if izlaz==1:
        pocetak_kviza()

    if izlaz==2:
        dodaj_pitanje()
    
    if izlaz==3:
        ispis_rezultata()

    if izlaz==0:
        break